package com.auto_clock_in.teamorange.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

import com.firebase.client.Firebase;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class AddEmployees extends AppCompatActivity {
    private TextView empName;
    private TextView empEmail;
    private TextView employeePassword;
    public FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_employees);
        empName = (TextView) findViewById(R.id.employeeName);
        empEmail = (TextView) findViewById(R.id.employeeEmail);
        employeePassword = (TextView) findViewById(R.id.employeePassword);
        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_emp, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //Add menu handling code
        switch (id) {
            case R.id.trackYourEmployeeHours:
                startActivity(new Intent(this,DisplayEmployee.class));
                break;
            case R.id.logOut:
                AddOffice.firebaseAuth.signOut();
                finish();
                AddOffice.office.finish();
                startActivity(new Intent(this, authActivity.class));
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public void addEmployee(View v) {
        if ((Objects.equals(empName.getText().toString(), "") || Objects.equals(empEmail.getText().toString(), ""))) {
            Toast.makeText(this, "Add correct details!", Toast.LENGTH_SHORT).show();
        } else {
            try {
                Firebase mRefChild1 = AddOffice.mRefChild.child("Employees");
                Map<String, Object> result = new HashMap<>();
                result.put("Name", empName.getText().toString());
                result.put("Email", empEmail.getText().toString());
                result.put("Work_hours",0);
                mRefChild1.child(empName.getText().toString()).setValue(result);
                mAuth.createUserWithEmailAndPassword(empEmail.getText().toString(), employeePassword.getText().toString());
                employeePassword.setText("");
                empEmail.setText("");
                empName.setText("");
                Toast.makeText(this,"Successfully added the employee to the database!",Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
            }

        }

    }
    public void showEmployee(View v)
    {
        startActivity(new Intent(this, DisplayEmployee.class));
    }
}
