package com.auto_clock_in.teamorange.myapplication;

import android.content.Intent;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

public class DisplayEmployee extends AppCompatActivity {
    List<Employee> emp;
    ListView Listviewemp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_employee);
        emp=new ArrayList<>();
        Listviewemp=(ListView)findViewById(R.id.Listviewemp);

    }

    @Override
    protected void onStart() {
        super.onStart();
        Firebase mRefChild1 = AddOffice.mRefChild.child("Employees");
        mRefChild1.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    emp.clear();
                    for (DataSnapshot e : dataSnapshot.getChildren()) {
                        Employee empl = e.getValue(Employee.class);
                        emp.add(empl);
                    }
                    EmployeeList addapter=new EmployeeList(DisplayEmployee.this,emp);
                    Listviewemp.setAdapter(addapter);
                }
                catch (Exception e)
                {
                }



}

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }
}
