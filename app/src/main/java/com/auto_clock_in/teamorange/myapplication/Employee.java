package com.auto_clock_in.teamorange.myapplication;

/**
 * Created by suman on 4/20/2017.
 */

public class Employee {
    String Email;
    String Name;
    String Work_hours;
    long timeStamp;
   public Employee()
    {

    }

    public Employee(String Email, String Name, String work_hours) {
        this.Email = Email;
        this.Name = Name;
        this.Work_hours = work_hours;
    }

    public String getEmpEmail() {
        return Email;
    }

    public String getEmpName() {
        return Name;
    }

    public String getWork_hours() {
        timeStamp = Long.parseLong(Work_hours);
        return secondsToTimeStamp(timeStamp);
    }
    public String secondsToTimeStamp(long totalSecs) {
        long hours = totalSecs / 3600;
        long minutes = (totalSecs % 3600) / 60;
        long seconds = totalSecs % 60;
        return String.format("%02d:%02d:%02d", hours, minutes, seconds);
    }
}
