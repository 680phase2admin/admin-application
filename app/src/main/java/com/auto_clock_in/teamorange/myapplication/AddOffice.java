package com.auto_clock_in.teamorange.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.google.android.gms.maps.GoogleMap;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.security.PrivateKey;
import java.util.Objects;

public class AddOffice extends AppCompatActivity implements View.OnClickListener {
    public static FirebaseAuth firebaseAuth;
    private TextView company;
    private TextView employee;
    private TextView wifiName;
    private Button addofficebtn;
    private Button gotoTrackbtn;
    public Firebase mRootRef;
    public static Firebase mRefChild;
    public static AddOffice office;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_office);
        firebaseAuth = FirebaseAuth.getInstance();
        company = (TextView) findViewById(R.id.companyName);
        employee = (TextView) findViewById(R.id.employeeNo);
        wifiName = (TextView) findViewById(R.id.wifiName);
        addofficebtn = (Button) findViewById(R.id.addofficebtn);
        gotoTrackbtn = (Button) findViewById(R.id.gotoTrackbtn);
        addofficebtn.setOnClickListener(this);
        gotoTrackbtn.setOnClickListener(this);
        Firebase.setAndroidContext(this);
        mRootRef = new Firebase("https://phase2-28a01.firebaseio.com/");
        if (firebaseAuth.getCurrentUser() == null) {
            finish();
            startActivity(new Intent(this, authActivity.class));
        } else {
            FirebaseUser user = firebaseAuth.getCurrentUser();
        }
        office = this;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_office, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //Add menu handling code
        switch (id) {
            case R.id.logOut:
                firebaseAuth.signOut();
                finish();
                startActivity(new Intent(this, authActivity.class));
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        if (v == addofficebtn) {
            if ((Objects.equals(employee.getText().toString(), "") || Objects.equals(wifiName.getText().toString(), "") || Objects.equals(company.getText().toString(), ""))) {
                Toast.makeText(this, "Add correct details!", Toast.LENGTH_SHORT).show();
            } else {
                try {
                    mRefChild = mRootRef.child(company.getText().toString());
                    Firebase mRefChild2 = mRefChild.child("Number of Employees");
                    mRefChild2.setValue(employee.getText().toString());
                    Firebase mRefChild3 = mRefChild.child("Wifi SSID");
                    mRefChild3.setValue(wifiName.getText().toString());
                    Toast.makeText(this, "Office details added!", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(this, MainActivity.class));
                } catch (Exception e) {
                    Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
                }

            }
        }
        if (v == gotoTrackbtn) {
            if (( Objects.equals(company.getText().toString(), ""))) {
                Toast.makeText(this, "Add correct Company name!", Toast.LENGTH_SHORT).show();
            } else {
                try {
                    mRefChild = mRootRef.child(company.getText().toString());
                    startActivity(new Intent(this, AddEmployees.class));
                } catch (Exception e) {
                    Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
                }

            }
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
