package com.auto_clock_in.teamorange.myapplication;

        import android.app.Activity;
        import android.support.annotation.NonNull;
        import android.support.annotation.Nullable;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.ArrayAdapter;
        import android.widget.EditText;
        import android.widget.TextView;

        import java.util.List;

/**
 * Created by suman on 4/20/2017.
 */

public class EmployeeList extends ArrayAdapter<Employee> {
    private Activity context;
    private List<Employee> emp;
    public EmployeeList(Activity context,List<Employee> emp)
    {
        super(context,R.layout.emp,emp);
        this.context=context;
        this.emp=emp;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater=context.getLayoutInflater();
        View listviewItem=inflater.inflate(R.layout.emp,null,true);
        //TextView employeeEmail=(TextView) listviewItem.findViewById(R.id.employeeEmail);
        TextView employeeName=(TextView) listviewItem.findViewById(R.id.employeeName);
        TextView workHours=(TextView) listviewItem.findViewById(R.id.workHours);
        Employee employee=emp.get(position);
       // employeeEmail.setText("Email: "+employee.getEmpEmail());
        employeeName.setText("Name: "+employee.getEmpName());
        workHours.setText("Hours Worked: "+employee.getWork_hours());
        return  listviewItem;
    }
}
